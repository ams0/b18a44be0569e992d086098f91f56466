## CKS notes December 2020

### General

Backup config files!!

alias k=kubectl

sudo runc --root /run/containerd/runc/k8s.io list

pstree -a

Keep containers up
    image: ubuntu
    command: ["/bin/bash", "-c", "sleep 3600"]

### Links
https://github.com/echoboomer/k8s-cks-notes
https://github.com/abdennour/certified-kubernetes-security-specialist
https://github.com/ibrahimjelliti/CKSS-Certified-Kubernetes-Security-Specialist

### Immutable pods ([ref](https://itnext.io/cks-exam-series-3-immutable-pods-3812cf76cff4))

    securityContext:
      readOnlyRootFilesystem: true

and add `emptyDir` volumes

### ImagePolicyWebhook

enable it in /etc/kubernetes/manifests/kube-apiserver.yaml
--admission-control=ImagePolicyWebhook,MutatingAdmissionWebhook
--admission-control-config-file=path-to-admission-config.yaml

MOUNT THE FILE in the /etc/kubernetes/manifests/kube-apiserver.yaml !!!
https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/#imagepolicywebhook
check the file with AdmissionConfiguration (given)

### PSP

enable it in /etc/kubernetes/manifests/kube-apiserver.yaml
spec:
containers:
- command:
- kube-apiserver
- --advertise-address=192.168.101.11
- --allow-privileged=true
- --anonymous-auth=true
- --authorization-mode=Node,RBAC
- --client-ca-file=/etc/kubernetes/pki/ca.crt
- --enable-admission-plugins=NodeRestriction,PodSecurityPolicy # change

Disable priv containers

apiVersion: policy/v1beta1
kind: PodSecurityPolicy
metadata:
  name: my-psp
spec:
  privileged: false  # Prevents creation of privileged Pods
  seLinux:
    rule: RunAsAny
  supplementalGroups:
    rule: RunAsAny
  runAsUser:
    rule: RunAsAny
  fsGroup:
    rule: RunAsAny
  volumes:
  - '*'


### Pods using secrets via SA ([ref](https://itnext.io/cks-exam-series-2-pods-and-secrets-3d92a6fba331))

Just create an SA in a namespace and use in the pod:

spec:
  serviceAccountName: secret-manager

it will have rights on secrets

### Fix Dockerfiles

### Create secret/mount in pod as vol
kubectl create secret generic my-secret --from-literal=key1=supersecret --from-literal=key2=topsecret

### AppArmor

apply profile with apparmor_parser <profile>
 sudo cat /sys/kernel/security/apparmor/profiles | sort

NOTE: LOCALHOST!!

  annotations:
    container.apparmor.security.beta.kubernetes.io/hello: localhost/k8s-apparmor-example-deny-write

### Network policy

Can combine both namespaceSelector and podselector:
spec:
  podSelector:
    matchLabels:
      app: postgres
  ingress:
  - from:
    - namespaceSelector: {}
      podSelector:
        matchLabels:
          app: admin
  policyTypes:
  - Ingress

### Audit logs

https://kubernetes.io/docs/tasks/debug-application-cluster/audit/#log-backend

Remember to mount the volume in /etc/kubernetes/manifests/kube-apiserver.yaml and apply the policy

# /etc/kubernetes/audit/policy.yaml
apiVersion: audit.k8s.io/v1
kind: Policy
rules:
# log Secret resources audits, level Metadata
- level: Metadata
resources:
- group: ""
resources: ["secrets"]
# log node related audits, level RequestResponse
- level: RequestResponse
userGroups: ["system:nodes"]
# for everything else don't log anything
- level: None

### CIS bench

master:
/etc/kubernetes/manifests/kube-apiserver.yaml
    - --authorization-mode=Node,RBAC

node
/etc/systemd/system/kubelet.service.d/10-kubeadm.conf

k delete po -n kube-system kube-apiserver-cks
sudo systemctl restart kubelet

### Falco scanning

### Container runtimeClass

kubelet args:

--container-runtime
--container-runtime-endpoint

You use spec.runtimeClassName to associate a Pod with a given RuntimeClass.

cat /etc/containerd/config.toml
disabled_plugins = ["restart"]
[plugins.linux]
shim = "/usr/local/bin/gvisor-containerd-shim"
shim_debug = true
[plugins.cri.containerd.runtimes.runsc]
runtime_type = "io.containerd.runtime.v1.linux"
runtime_engine = "/usr/bin/runsc"
runtime_root = "/run/containerd/runsc"

# /opt/course/10/arguments
--container-runtime remote
--container-runtime-endpoint unix:///run/containerd/containerd.sock