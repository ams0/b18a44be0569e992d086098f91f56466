
#Create storage
RG=minio
export AZURE_STORAGE_ACCOUNT=azureminio
az group create -g $RG
az storage account create -g $RG -n $AZURE_STORAGE_ACCOUNT

export AZURE_STORAGE_KEY=`az storage account keys list -g $RG -n $AZURE_STORAGE_ACCOUNT --query "[0].value" -o tsv`

#Install Minio
helm repo add minio https://helm.min.io/

#note: defaultBucket.name cannot be "minio"!
helm upgrade --install --namespace minio --create-namespace minio minio/minio \
 --set azuregateway.enabled=true \
 --set azuregateway.replicas=1 \
 --set defaultBucket.enabled=true --set defaultBucket.name=default \
 --set accessKey=$AZURE_STORAGE_ACCOUNT,secretKey=$AZURE_STORAGE_KEY

export POD_NAME=$(kubectl get pods --namespace minio -l "release=minio" -o jsonpath="{.items[0].metadata.name}"); kubectl port-forward $POD_NAME 9000 --namespace minio

open localhost:9000

#test pod access
kubectl run minioclient --image minio/mc --command -- sleep 3600
kubectl exec minioclient mc config host add myminio http://minio.minio:9000 $AZURE_STORAGE_ACCOUNT $AZURE_STORAGE_KEY
kubectl exec minioclient mc ls myminio/
kubectl delete po minioclient